# Friendica styles

Easy On The Eyes is written for use with the Vier theme.

I'm on Nerdica, but EOTE (and any other Friendica style I might write) should work on 
other instances as well. Just change the domain in the rule 
```@-moz-document domain("nerdica.net")``` to your own instance's domain.