# These styles are not being maintained.

I don't use Twitter anymore, so I can't keep up with any changes they might make to 
Tweetdeck's styling. I won't accept issues or pull requests for these styles.

Feel free to take them and modify them however you want.